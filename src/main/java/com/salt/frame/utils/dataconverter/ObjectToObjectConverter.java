package com.salt.frame.utils.dataconverter;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ObjectToObjectConverter extends ModelMapper {

	public <D> D convert(Object source, Class<D> targetClass) {

		try {
			getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			getConfiguration().setSkipNullEnabled(true);
			return map(source, targetClass);

		} catch (Exception e) {
			log.debug("conversion error", e);
			throw e;
		}
	}

	public <D> List<D> convertList(final List<?> source, final Class<D> targetClass) {

		List<D> target = new ArrayList<D>();

		getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		getConfiguration().setSkipNullEnabled(true);
		for (Object s : source)
			target.add(map(s, targetClass));

		return target;
	}

	public void convert(Object source, Object target) {

		try {
			getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
			getConfiguration().setSkipNullEnabled(true);
			map(source, target);

		} catch (Exception e) {
			log.debug("conversion error", e);
			throw e;
		}
	}
}
