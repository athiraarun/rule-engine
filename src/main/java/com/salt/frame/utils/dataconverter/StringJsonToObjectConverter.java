package com.salt.frame.utils.dataconverter;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class StringJsonToObjectConverter {
	public static <T> T convert(String input, Class<T> targetClass) throws Exception {
		T result = null;
		try {
		 result = new ObjectMapper().readValue(input, targetClass);
		}catch (Exception e) {
			log.debug("error in object mapper {}", e);
		}
		return result;
	}

//	public static Object convertModel(String input, String model) throws Exception {
//		Object result = null;
//		Class<?> classTemp = Class.forName(model);
//		try {
//		 result = new ObjectMapper().readValue(input, classTemp);
//		}catch (Exception e) {
//			logger.debug("error in object mapper {}", e);
//		}
//		return result;
//	}
}
