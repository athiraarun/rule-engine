package com.salt.frame.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class FrameConfigUtils {
    public static final int AUDIO_BITRATE = 128000;
    public static final int AUDIO_CHANNELS = 2;
    public static final int AUDIO_SAMPLING_RATE = 44100;
    public static final int VIDEO_BITRATE = 160000;
    public static final int VIDEO_FRAME_RATE = 20; // More the frames, more quality and size. Standard frame rate is 24fps.
    // Default dimension for a HD video is 1280*720 pixels
    public static final int VIDEO_WIDTH = 1280;
    public static final int VIDEO_HEIGHT = 720;




}
