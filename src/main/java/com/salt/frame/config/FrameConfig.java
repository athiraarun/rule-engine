package com.salt.frame.config;

import org.springframework.context.annotation.Configuration;

@Configuration
public class FrameConfig {

//	public static final String COLDEF_SHORT = "NUMBER(5,0)";
//	public static final String COLDEF_INT = "NUMBER(10,0)";
//	public static final String COLDEF_LONG = "NUMBER(19,0)";
//	public static final String COLDEF_DOUBLE = "NUMBER(19,4)";
//	public static final String COLDEF_DECIMAL = "NUMBER(19,2)";
//	public static final String COLDEF_BOOLEAN = "NUMBER(1,0)";
//	public static final String COLDEF_TEXT = "VARCHAR2(4000)";
//	public static final String COLDEF_MTEXT = "VARCHAR2(500)";

	// MYSQL (DEV / clientqa)
	public static final String COLDEF_SHORT = "SMALLINT";
	public static final String COLDEF_INT = "INT";
	public static final String COLDEF_LONG = "BIGINT";
	public static final String COLDEF_DOUBLE = "DOUBLE";
	public static final String COLDEF_DECIMAL = "DECIMAL(19,2)";
	public static final String COLDEF_DECIMAL_PERCENTAGE = "DECIMAL(5,2)";
	public static final String COLDEF_BOOLEAN = "BIT";
	public static final String COLDEF_BLOB = "BLOB";
	public static final String COLDEF_LBLOB = "LONGBLOB";
	public static final String COLDEF_TEXT = "TEXT";
	public static final String COLDEF_MTEXT = "MEDIUMTEXT";
	public static final String COLDEF_LTEXT = "LONGTEXT";

}
