package com.salt.frame.config;

public class FWEXCEPTIONS {
	// UNKOWN
	public static final String EXCEPTION_UNKNOWN = "TRUNK001";

	// SECURITY
	public static final String EXCEPTION_SECURE_BADCRED = "TRSEC001";
	public static final String EXCEPTION_SECURE_BADTKN = "TRSEC002";

	// SPRINGFRAMEWORK
	public static final String EXCEPTION_SPRING_UNKNOWN = "TRSPR001";

	// TECHNICAL
	public static final String EXCEPTION_TECH_UNKNOWN = "TRTER001";

	// DATABASE
	public static final String EXCEPTION_DB_UNKNOWN = "TRDBS001";
	public static final String EXCEPTION_DB_PARTUPDATE = "TRDBS002";
	public static final String EXCEPTION_DB_NONUNIQUE = "TRDBS003";
	public static final String EXCEPTION_DB_DATAINTEGRITY = "TRDBS004";
	public static final String EXCEPTION_DB_NODATAFOUND = "TRDBS005";
	
	//COMMUNICATION
	public static final String EXCEPTION_EMAIL_SETUP = "TRCOM001";
	public static final String EXCEPTION_SMS_SETUP = "TRCOM002";
	
	// CONVERTER
	public static final String EXCEPTION_CONVERTER_UNKNOWN = "TRCONV001";
	
	
}
