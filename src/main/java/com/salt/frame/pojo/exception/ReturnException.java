package com.salt.frame.pojo.exception;

import org.springframework.http.HttpStatus;

import com.salt.frame.config.FWEXCEPTIONS;
import com.salt.frame.pojo.ReturnValue;

import lombok.extern.slf4j.Slf4j;



@Slf4j
public class ReturnException<E extends Exception> extends ReturnValue {

	public ReturnException(Exception e) {
		super(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_UNKNOWN, "Ooopps! What happened?",
				"Sorry! We have hit an unexpected Exception", "Please try again");
		log.error("UNKOWN Exception", e);
	}

	public ReturnException(FrameException e) {
		super(e.getHttpStatus(), e.getExceptionCode(), e.getExceptionHeader(), e.getExceptionMessage(), e.getSolution());
		log.error("CODE:{} Details:{}", e.getExceptionCode(), e.toString());
	}

	public ReturnException(String errHeader, String errMessage, String solution, Object data) {
		super(HttpStatus.INTERNAL_SERVER_ERROR, errHeader, errMessage, solution, data);
	}

	public ReturnException(String errHeader, String errMessage, String solution) {
		super(HttpStatus.INTERNAL_SERVER_ERROR, errHeader, errMessage, solution, null);
	}

	public ReturnException(String errHeader, String errMessage) {
		super(HttpStatus.INTERNAL_SERVER_ERROR, errHeader, errMessage, "No known solution", null);
	}
	
	public ReturnException(String s) {
		super(HttpStatus.INTERNAL_SERVER_ERROR, "Error", s, null);
	}


	
}
