package com.salt.frame.pojo.exception;

import org.springframework.http.HttpStatus;

import com.salt.frame.config.FWEXCEPTIONS;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/*
 * Accaptable HTTPStatuses OK(200, "OK") BAD_REQUEST(400, "Bad Request")
 * UNAUTHORIZED(401, "Unauthorized") PAYMENT_REQUIRED(402, "Payment Required")
 * FORBIDDEN(403, "Forbidden") NOT_FOUND(404, "Not Found") REQUEST_TIMEOUT(408,
 * "Request Timeout") PAYLOAD_TOO_LARGE(413, "Payload Too Large")
 * UNSUPPORTED_MEDIA_TYPE(415, "Unsupported Media Type") LOCKED(423, "Locked")
 * INTERNAL_SERVER_ERROR(500, "Internal Server Error") =====>>> FALLBACK
 * NOT_IMPLEMENTED(501, "Not Implemented")
 * 
 */
@Getter
@Setter
@ToString
public class FrameException extends Exception {

	private static final long serialVersionUID = -7521949620835645458L;

	private HttpStatus httpStatus;
	private String exceptionCode;
	private String exceptionHeader;
	private String exceptionMessage;
	private String solution;

	public FrameException(String solution) {
		this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		this.exceptionCode = FWEXCEPTIONS.EXCEPTION_UNKNOWN;
		this.exceptionHeader = "Ooopps! Not sure what happened...";
		this.exceptionMessage = "Sorry! We have hit an unexpected Exception";
		this.solution = solution;
	}

	public FrameException() {
		this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		this.exceptionCode = FWEXCEPTIONS.EXCEPTION_UNKNOWN;
		this.exceptionHeader = "Ooopps! Not sure what happened...";
		this.exceptionMessage = "Sorry! We have hit an unexpected Exception";
		this.solution = "Please try again";

	}

	public FrameException(HttpStatus httpStatus, String exceptionCode, String errorHeader, String errorMessage,
			String solution) {
		this.httpStatus = httpStatus;
		this.exceptionCode = exceptionCode;
		this.exceptionHeader = errorHeader;
		this.exceptionMessage = errorMessage;
		this.solution = solution;
	}
}
