package com.salt.frame.pojo.exception;

import org.springframework.http.HttpStatus;

import com.salt.frame.pojo.ReturnValue;

public class ReturnSuccess<T> extends ReturnValue{

	public ReturnSuccess(T returnValue) {
		super(HttpStatus.OK, null, null, null, null, returnValue);
	}
	
	public ReturnSuccess() {
		super(HttpStatus.OK, null, null, null);
	}



}
