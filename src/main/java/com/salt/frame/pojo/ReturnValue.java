package com.salt.frame.pojo;

import org.springframework.http.HttpStatus;

import com.salt.frame.config.FWEXCEPTIONS;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter@Getter
@ToString
public class ReturnValue {
	private HttpStatus status = HttpStatus.OK;
	private String errCode;
	private String errHeader;
	private String errMessage;
	private String solution;
	private Object data;

	public ReturnValue(HttpStatus status, String errCode, String errHeader, String errMessage, String solution, Object data) {
		this.status = status;
		this.errCode = errCode;
		this.errHeader = errHeader;
		this.errMessage = errMessage;
		this.solution = solution;
		this.data = data;
	}
	public ReturnValue(HttpStatus status, String errHeader, String errMessage, String solution, Object data) {
		this.status = status;
		this.errCode = FWEXCEPTIONS.EXCEPTION_UNKNOWN;
		this.errHeader = errHeader;
		this.errMessage = errMessage;
		this.solution = solution;
		this.data = data;
	}
	
	public ReturnValue(HttpStatus status, String errHeader, String errMessage, Object data) {
		this.status = status;
		this.errHeader = errHeader;
		this.errMessage = errMessage;
		this.data = data;
	}

}

