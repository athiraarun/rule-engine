package com.salt.frame.repo.dao.base;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.http.HttpStatus;

import com.salt.frame.config.FWEXCEPTIONS;
import com.salt.frame.pojo.exception.FrameException;
import com.salt.frame.repo.entity.FrameEntity;

import lombok.extern.slf4j.Slf4j;

@NoRepositoryBean
@Slf4j
public abstract class FrameDao<T extends FrameEntity> {

	@PersistenceContext
	protected EntityManager em;
	protected Class<T> entity;

	@PostConstruct
	protected abstract void setEntity();

	public Class<T> getEntity() {
		return entity;
	}

	public EntityManager getEntityManager() {
		return em;
	}

	public T find(long id) throws FrameException {
		try {
			return (T) getEntityManager().find(entity, id);
		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN, "Data Error",
					"The find key is erroneous", "There is an error in the data");
		}
	}

	public T find(int id) throws FrameException {
		try {
			return (T) getEntityManager().find(entity, id);
		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN, "Data Error",
					"The find key is erroneous", "There is an error in the data");
		}
	}

	public T find(String id) throws FrameException {
		try {
			return (T) getEntityManager().find(entity, id);
		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN, "Data Error",
					"The find key is erroneous", "There is an error in the data");
		}
	}

	@SuppressWarnings("unchecked")
	public List<T> findAll() throws FrameException {
		try {
			return (List<T>) getEntityManager().createQuery("from " + entity.getName()).getResultList();

		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		}
	}

	public List<T> findAllValid() throws FrameException {
		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<T> cq = cb.createQuery(entity);
		Root<T> from = cq.from(entity);
		Predicate pred = cb.between(cb.literal(new Date()), from.<Date>get("validFrom"), from.<Date>get("validTo"));
		List<T> result = null;

		try {
			cq.select(from).where(pred);
			TypedQuery<T> q = em.createQuery(cq);

			result = q.getResultList();
		} catch (QueryTimeoutException qt) {
			log.error("More than one record found", qt);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_PARTUPDATE,
					"Query Timeout", "The query timed out",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (PersistenceException thrw) {
			log.error("DB Error", thrw);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in behaviour",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		}

		return result;
	}

	public T save(T entity) throws FrameException {
		try {
			return (T) getEntityManager().merge(entity);
		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data", "Please report this");
		}
	}

	public void delete(T entity) throws FrameException {
		try {
			T mergedEntity = getEntityManager().merge(entity);
			getEntityManager().remove(mergedEntity);
		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		}
	}

	public void deleteById(long entityId) throws FrameException {
		T entity = find(entityId);
		delete(entity);
	}

	public List<T> save(List<T> entities) throws FrameException {
		List<T> rentities = new ArrayList<T>();
		for (T entitity : entities)
			rentities.add(save(entitity));

		return rentities;
	}

	public void delete(List<T> entities) throws FrameException {
		for (T entitity : entities)
			delete(entitity);
	}

	public T get(String name, String value) throws FrameException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<T> cq = cb.createQuery(entity);
		Root<T> from = cq.from(entity);

		cq.select(from).where(cb.equal(from.get(name), value));
		TypedQuery<T> q = em.createQuery(cq);

		T t = null;
		try {
			t = q.getSingleResult();

		} catch (NoResultException sw1) {
			log.info("Swallowed DB Error({}): record not found", entity.getName());

		} catch (NonUniqueResultException nu) {
			log.error("More than one record found", nu);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_NONUNIQUE,
					"Data Inconsistency", "There is a inconsistency in data", "Please report this error");

		} catch (QueryTimeoutException qt) {
			log.error("More than one record found", qt);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_PARTUPDATE,
					"Query Timeout", "The query timed out",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (PersistenceException thrw) {
			log.error("DB Error", thrw);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in behaviour",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		}

		return t;
	}

	public T get(Map<String, String> params) throws FrameException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		List<Predicate> preds = new ArrayList<Predicate>();

		CriteriaQuery<T> cq = cb.createQuery(entity);
		Root<T> from = cq.from(entity);

		for (Map.Entry<String, String> param : params.entrySet()) {
			preds.add(createPredicate(cb, from, param));
		}
		cq.select(from).where(preds.toArray(new Predicate[] {}));
		TypedQuery<T> q = em.createQuery(cq);

		T t = null;
		try {
			t = q.getSingleResult();

		} catch (NoResultException sw1) {
			log.info("Swallowed DB Error({}): record not found", entity.getName());

		} catch (NonUniqueResultException nu) {
			log.error("More than one record found", nu);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_NONUNIQUE,
					"Data Inconsistency", "There is a inconsistency in data", "Please report this error");

		} catch (QueryTimeoutException qt) {
			log.error("More than one record found", qt);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_PARTUPDATE,
					"Query Timeout", "The query timed out",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (PersistenceException thrw) {
			log.error("DB Error", thrw);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in behaviour",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		}

		return t;
	}

	public List<T> getList(String name, String value) throws FrameException {
		CriteriaBuilder cb = em.getCriteriaBuilder();

		CriteriaQuery<T> cq = cb.createQuery(entity);
		Root<T> from = cq.from(entity);

		cq.select(from).where(cb.equal(from.get(name), value));
		TypedQuery<T> q = em.createQuery(cq);

		List<T> ts = null;
		try {
			ts = q.getResultList();

		} catch (QueryTimeoutException qt) {
			log.error("More than one record found", qt);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_PARTUPDATE,
					"Query Timeout", "The query timed out",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (PersistenceException thrw) {
			log.error("DB Error", thrw);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in behaviour",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		}

		return ts;
	}

	public List<T> getListAscending(String name, String value) throws FrameException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		List<Order> order = new ArrayList<Order>();

		CriteriaQuery<T> cq = cb.createQuery(entity);
		Root<T> from = cq.from(entity);

		cq.select(from).where(cb.equal(from.get(name), value));
		order.add(cb.asc(from.get(name)));
		cq.orderBy(order);
		TypedQuery<T> q = em.createQuery(cq);

		List<T> ts = null;
		try {
			ts = q.getResultList();

		} catch (QueryTimeoutException qt) {
			log.error("More than one record found", qt);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_PARTUPDATE,
					"Query Timeout", "The query timed out",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (PersistenceException thrw) {
			log.error("DB Error", thrw);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in behaviour",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		}

		return ts;
	}

	public List<T> getListDescending(String name, String value) throws FrameException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		List<Order> order = new ArrayList<Order>();

		CriteriaQuery<T> cq = cb.createQuery(entity);
		Root<T> from = cq.from(entity);

		cq.select(from).where(cb.equal(from.get(name), value));
		order.add(cb.desc(from.get(name)));
		cq.orderBy(order);
		TypedQuery<T> q = em.createQuery(cq);

		List<T> ts = null;
		try {
			ts = q.getResultList();

		} catch (QueryTimeoutException qt) {
			log.error("More than one record found", qt);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_PARTUPDATE,
					"Query Timeout", "The query timed out",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (PersistenceException thrw) {
			log.error("DB Error", thrw);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in behaviour",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		}

		return ts;
	}

	public List<T> getList(Map<String, String> params) throws FrameException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		List<Predicate> preds = new ArrayList<Predicate>();

		CriteriaQuery<T> cq = cb.createQuery(entity);
		Root<T> from = cq.from(entity);

		for (Map.Entry<String, String> param : params.entrySet())
			preds.add(createPredicate(cb, from, param));

		cq.select(from).where(preds.toArray(new Predicate[] {}));
		TypedQuery<T> q = em.createQuery(cq);

		List<T> ts = null;
		try {
			ts = q.getResultList();

		} catch (QueryTimeoutException qt) {
			log.error("More than one record found", qt);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_PARTUPDATE,
					"Query Timeout", "The query timed out",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (PersistenceException thrw) {
			log.error("DB Error", thrw);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in behaviour",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		}

		return ts;
	}

	public List<T> getListAscending(Map<String, String> params, List<String> orderBy) throws FrameException {
		log.info("Accsess 'getAscending' @ {}", new Date());
		CriteriaBuilder cb = em.getCriteriaBuilder();
		List<Predicate> preds = new ArrayList<Predicate>();
		List<Order> order = new ArrayList<Order>();

		CriteriaQuery<T> cq = cb.createQuery(entity);
		Root<T> from = cq.from(entity);

		for (Map.Entry<String, String> param : params.entrySet())
			preds.add(createPredicate(cb, from, param));

		for (String field : orderBy)
			order.add(cb.asc(from.get(field)));

		cq.select(from).where(preds.toArray(new Predicate[] {}));
		cq.orderBy(order);

		TypedQuery<T> q = em.createQuery(cq);

		List<T> ts = null;
		try {
			ts = q.getResultList();

		} catch (QueryTimeoutException qt) {
			log.error("More than one record found", qt);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_PARTUPDATE,
					"Query Timeout", "The query timed out",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (PersistenceException thrw) {
			log.error("DB Error", thrw);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in behaviour",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		}

		return ts;
	}

	public List<T> getListDescending(Map<String, String> params, List<String> orderBy) throws FrameException {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		List<Predicate> preds = new ArrayList<Predicate>();
		List<Order> order = new ArrayList<Order>();

		CriteriaQuery<T> cq = cb.createQuery(entity);
		Root<T> from = cq.from(entity);

		for (Map.Entry<String, String> param : params.entrySet())
			preds.add(createPredicate(cb, from, param));

		for (String field : orderBy)
			order.add(cb.desc(from.get(field)));

		cq.select(from).where(preds.toArray(new Predicate[] {}));
		cq.orderBy(order);

		TypedQuery<T> q = em.createQuery(cq);

		List<T> ts = null;
		try {
			ts = q.getResultList();

		} catch (QueryTimeoutException qt) {
			log.error("More than one record found", qt);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_PARTUPDATE,
					"Query Timeout", "The query timed out",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (PersistenceException thrw) {
			log.error("DB Error", thrw);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in behaviour",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		}

		return ts;
	}

	private Predicate createPredicate(final CriteriaBuilder cb, final Root<?> from,
			final Map.Entry<String, String> param) {

		String valueType;
		
		int lastIndex = param.getValue().lastIndexOf(':') + 1;

		if (lastIndex > 0)
			valueType = param.getValue().substring(lastIndex);
		else
			return cb.like(from.get(param.getKey()), param.getValue());
		switch (valueType.toUpperCase()) {
		case "(BOOLEAN)":
		case "(BOOL)":
			return cb.equal(from.get(param.getKey()), Boolean.parseBoolean(param.getValue().substring(0, lastIndex-1)));
		case "(INTEGER)":
		case "(INT)":
			return cb.equal(from.get(param.getKey()), Integer.parseInt(param.getValue().substring(0, lastIndex-1)));
		case "(SHORT)":
			return cb.equal(from.get(param.getKey()), Short.parseShort(param.getValue().substring(0, lastIndex-1)));
		case "(LONG)":
			return cb.equal(from.get(param.getKey()), Long.parseLong(param.getValue().substring(0, lastIndex-1)));
		case "(FLOAT)":
			return cb.equal(from.get(param.getKey()), Float.parseFloat(param.getValue().substring(0, lastIndex-1)));
		case "(DOUBLE)":
			return cb.equal(from.get(param.getKey()), Double.parseDouble(param.getValue().substring(0, lastIndex-1)));
		case "(BIGDECIMAL)":
		case "(BIGD)":
			return cb.equal(from.get(param.getKey()), new BigDecimal(param.getValue().substring(0, lastIndex-1)));
//		case "(DATE)":
//			return cb.equal(from.get(param.getKey()), Date.parse(param.getValue()));
		default:
			return null;
		}
	}
}