package com.salt.frame.repo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.salt.frame.config.FrameConfig;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public class FrameEntity {

	@Id
	@Column(name = "id", columnDefinition = FrameConfig.COLDEF_LONG)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Getter @Setter
	private Long id;

	@CreationTimestamp
	@Column(name = "created_timestamp", nullable = false, updatable=false)
	private Date createdTimestamp;

	@CreatedBy
	@Column(name = "created_by", length = 20 , nullable = false, updatable=false)
	private String createdBy;

	@UpdateTimestamp
	@Column(name = "last_updated_timestamp", nullable = false)
	private Date lastUpdatedTimestamp;

	@LastModifiedBy
	@Column(name = "last_updated_by", length = 20, nullable = false)
	private String lastUpdatedBy;

}