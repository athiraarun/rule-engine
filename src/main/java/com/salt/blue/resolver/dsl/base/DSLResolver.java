package com.salt.blue.resolver.dsl.base;

import com.salt.frame.pojo.exception.FrameException;

public interface DSLResolver {
    String getResolverKeyword();
    <I> Object resolveValue(String keyword,I inputData) throws FrameException;
}
