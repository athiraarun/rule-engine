package com.salt.blue.resolver.dsl;

import org.springframework.stereotype.Component;

import com.salt.blue.resolver.dsl.base.DSLResolver;

@Component
public class WatchedVideoTransactionResolver implements DSLResolver {
    private static final String RESOLVER_KEYWORD = "video";
    private static final String WATCHED_DAILY = "watched_daily";
    private static final String COINS_ACCRUED = "coins_accrued";

    @Override
    public String getResolverKeyword() {
        return RESOLVER_KEYWORD;
    }

    @Override
    public <I> Object resolveValue(String keyword,I inputData) {
        if (keyword.equalsIgnoreCase(WATCHED_DAILY)){
            //code to check video watched daily
            return true;
        }
        
        if (keyword.equalsIgnoreCase(COINS_ACCRUED)){
            //code to check user already earned max rewards for this transaction
            return false;
        }

        return null;
    }

}
