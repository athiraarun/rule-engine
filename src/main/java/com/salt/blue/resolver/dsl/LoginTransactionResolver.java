package com.salt.blue.resolver.dsl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.salt.blue.pojo.RewardInputDetails;
import com.salt.blue.repo.dao.LoginAuditDao;
import com.salt.blue.resolver.dsl.base.DSLResolver;
import com.salt.frame.pojo.exception.FrameException;

@Component
public class LoginTransactionResolver implements DSLResolver {
	@Autowired
	private LoginAuditDao loginAuditDao;
	private static final String RESOLVER_KEYWORD = "login";
	private static final String FIRST_LOGIN_IN_ENROLLMENT_PERIOD = "first_login";
	private static final String DAILY_LOGIN = "daily_login";
//	private static final String ACCRUAL_DATE = "accrual_date";
//	private static final String USER_ID = "user_id";

	@Override
	public String getResolverKeyword() {
		return RESOLVER_KEYWORD;
	}

	@Override
	public <I> Object resolveValue(String keyword, I inputData) throws FrameException {
		// TODO check the first date is within the enrollment login period
		if (keyword.equalsIgnoreCase(FIRST_LOGIN_IN_ENROLLMENT_PERIOD)) {
			RewardInputDetails rewardInputData = (RewardInputDetails) inputData;
			if (loginAuditDao.findByUserId(rewardInputData.getUserId()).isEmpty()) {
				return true;
			}
			else
				return false;
		}

		if (keyword.equalsIgnoreCase(DAILY_LOGIN)) {
			// Code to check whether the user login daily
			return false;
		}

//		if (keyword.equalsIgnoreCase(ACCRUAL_DATE)) {
//			// Code to check whether the user login daily
//			return new Date();
//		}
//		if (keyword.equalsIgnoreCase(USER_ID)) {
//			RewardInputDetails rewardInputData = (RewardInputDetails) inputData;
//			return rewardInputData.getUserId();
//		}

		return null;
	}

}
