package com.salt.blue.resolver.dsl;

import org.springframework.stereotype.Component;

import com.salt.blue.resolver.dsl.base.DSLResolver;

@Component
public class RewardCoinResolver implements DSLResolver {
	private static final String RESOLVER_KEYWORD = "coin";
	private static final String GOLD = "gold";
	private static final String SILVER = "silver";
	private static final String BRONZE = "bronze";
	private static final String PLATINUM = "platinum";

	@Override
	public String getResolverKeyword() {
		return RESOLVER_KEYWORD;
	}

	@Override
	public <I> Object resolveValue(String keyword, I inputData) {

		switch (keyword) {
		case GOLD:
			return 1L;
			
		case SILVER:
			return 2L;
			
		case BRONZE:
			return 3L;
			
		case PLATINUM:
			return 4L;

		default:
			return null;
		}

	}
}
