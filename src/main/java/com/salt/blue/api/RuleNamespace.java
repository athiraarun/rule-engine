package com.salt.blue.api;

public enum RuleNamespace {
    LOAN,
    DEFAULT,
    INSURANCE,
    REWARD
}
