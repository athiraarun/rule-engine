package com.salt.blue.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Enums;
import com.salt.blue.macro.RewardsService;
import com.salt.blue.pojo.RewardInputDetails;
import com.salt.blue.pojo.RewardOutputDetails;
import com.salt.blue.pojo.RewardsAccrualRulesDto;
import com.salt.blue.ruleengine.RewardInferenceEngine;
import com.salt.blue.ruleengine.base.RuleEngine;
import com.salt.blue.rulesImpl.insuranceRuleEngine.InsuranceDetails;
import com.salt.blue.rulesImpl.insuranceRuleEngine.InsuranceInferenceEngine;
import com.salt.blue.rulesImpl.insuranceRuleEngine.PolicyHolderDetails;
import com.salt.blue.rulesImpl.loanRuleEngine.LoanDetails;
import com.salt.blue.rulesImpl.loanRuleEngine.LoanInferenceEngine;
import com.salt.blue.rulesImpl.loanRuleEngine.UserDetails;
import com.salt.frame.pojo.exception.FrameException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class RuleEngineRestController {
    @Autowired
    private RewardsService knowledgeBaseService;
    @Autowired
    private RuleEngine ruleEngine;
    @Autowired
    private LoanInferenceEngine loanInferenceEngine;
    @Autowired
    private RewardInferenceEngine rewardInferenceEngine;
    @Autowired
    private InsuranceInferenceEngine insuranceInferenceEngine;

    @GetMapping(value = "/get-all-rules/{ruleNamespace}")
    public ResponseEntity<?> getRulesByNamespace(@PathVariable("ruleNamespace") String ruleNamespace) throws FrameException {
        RuleNamespace namespace = Enums.getIfPresent(RuleNamespace.class, ruleNamespace.toUpperCase()).or(RuleNamespace.DEFAULT);
        List<RewardsAccrualRulesDto> allRules = knowledgeBaseService.getAllRuleByNamespace(namespace.toString());
        return ResponseEntity.ok(allRules);
    }

    @GetMapping(value = "/get-all-rules")
    public ResponseEntity<?> getAllRules() throws FrameException {
        List<RewardsAccrualRulesDto> allRules = knowledgeBaseService.getAllRules();
        return ResponseEntity.ok(allRules);
    }

    @PostMapping(value = "/loan")
    public ResponseEntity<?> postUserLoanDetails(@RequestBody UserDetails userDetails) throws FrameException {
        LoanDetails result = (LoanDetails) ruleEngine.run(loanInferenceEngine, userDetails);
        return ResponseEntity.ok(result);
    }
    
    @PostMapping(value = "/reward")
    public ResponseEntity<?> postRewardDetails(@RequestBody RewardInputDetails rewardInputDetails) throws FrameException {
    	RewardOutputDetails result = (RewardOutputDetails)ruleEngine.run(rewardInferenceEngine, rewardInputDetails);
        return ResponseEntity.ok(result);
    }

    @PostMapping(value = "/insurance")
    public ResponseEntity<?> postCarLoanDetails(@RequestBody PolicyHolderDetails policyHolderDetails) throws FrameException {
        InsuranceDetails result = (InsuranceDetails) ruleEngine.run(insuranceInferenceEngine, policyHolderDetails);
        return ResponseEntity.ok(result);
    }
}
