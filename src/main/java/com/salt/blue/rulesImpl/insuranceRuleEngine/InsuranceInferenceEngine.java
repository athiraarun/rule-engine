package com.salt.blue.rulesImpl.insuranceRuleEngine;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import com.salt.blue.api.RuleNamespace;
import com.salt.blue.ruleengine.base.InferenceEngine;

@Slf4j
@Service
public class InsuranceInferenceEngine extends InferenceEngine<PolicyHolderDetails, InsuranceDetails> {

    @Override
    protected RuleNamespace getRuleNamespace() {
        return RuleNamespace.INSURANCE;
    }

    @Override
    protected InsuranceDetails initializeOutputResult() {
        return InsuranceDetails.builder().build();
    }
}
