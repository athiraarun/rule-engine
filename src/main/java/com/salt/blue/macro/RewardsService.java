package com.salt.blue.macro;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.salt.blue.pojo.RewardsAccrualRulesDto;
import com.salt.blue.repo.dao.RewardsAccrualRulesDao;
import com.salt.blue.repo.entity.RewardsAccrualRulesEntity;
import com.salt.frame.pojo.exception.FrameException;
import com.salt.frame.utils.dataconverter.ObjectToObjectConverter;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RewardsService {
	@Autowired
	private ObjectToObjectConverter objConverter;
	@Autowired
	private RewardsAccrualRulesDao rulesDao;

	public List<RewardsAccrualRulesDto> getAllRules() throws FrameException {
		log.info("Accessed getAllRules @ {}", new Date());

		List<RewardsAccrualRulesDto> accrualRulesDtos = null;
		List<RewardsAccrualRulesEntity> rulesEntities = rulesDao.findAll();
		if (!rulesEntities.isEmpty())
			accrualRulesDtos = objConverter.convertList(rulesEntities, RewardsAccrualRulesDto.class);

		log.debug("rulesEntities: {}", rulesEntities);
		log.info("Exited 'getAllRules' @ {}", new Date());
		return accrualRulesDtos;

	}

	public List<RewardsAccrualRulesDto> getAllRuleByNamespace(String ruleNamespace) throws FrameException {
		log.info("Accessed getAllRuleByNamespace @ {}", new Date());

		List<RewardsAccrualRulesDto> accrualRulesDtos = null;
		List<RewardsAccrualRulesEntity> rulesEntities = rulesDao.getList("ruleNamespace", ruleNamespace);
		if (!rulesEntities.isEmpty())
			accrualRulesDtos = objConverter.convertList(rulesEntities, RewardsAccrualRulesDto.class);

		log.debug("rulesEntities: {}", rulesEntities);
		log.info("Exited 'getAllRuleByNamespace' @ {}", new Date());
		return accrualRulesDtos;
	}

}
