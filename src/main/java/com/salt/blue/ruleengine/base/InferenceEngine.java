package com.salt.blue.ruleengine.base;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.salt.blue.api.RuleNamespace;
import com.salt.blue.parser.RuleParser;
import com.salt.blue.pojo.RewardsAccrualRulesDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public abstract class InferenceEngine<I, O> {

    @Autowired
    private RuleParser<I, O> ruleParser;

    /**
     * Run inference engine on set of rules for given data.
     * @param listOfRules
     * @param inputData
     * @return
     */
    public O run (List<RewardsAccrualRulesDto> listOfRules, I inputData){
        if (null == listOfRules || listOfRules.isEmpty()){
            return null;
        }

        //STEP 1 (MATCH) : Match the facts and data against the set of rules.
        List<RewardsAccrualRulesDto> conflictSet = match(listOfRules, inputData);

        //STEP 2 (RESOLVE) : Resolve the conflict and give the selected one rule.
        RewardsAccrualRulesDto resolvedRule = resolve(conflictSet);
        if (null == resolvedRule){
            return null;
        }

        //STEP 3 (EXECUTE) : Run the action of the selected rule on given data and return the output.
        O outputResult = executeRule(resolvedRule, inputData);

        return outputResult;
    }

    /**
     *We can use here any pattern matching algo:
     * 1. Rete
     * 2. Linear
     * 3. Treat
     * 4. Leaps
     *
     * Here we are using Linear matching algorithm for pattern matching.
     * @param listOfRules
     * @param inputData
     * @return
     */
    protected List<RewardsAccrualRulesDto> match(List<RewardsAccrualRulesDto> listOfRules, I inputData){
        return listOfRules.stream()
                .filter(
                        rule -> {
                            String condition = rule.getEvaluationExpression();
                            return ruleParser.parseCondition(condition, inputData);
                        }
                )
                .collect(Collectors.toList());
    }

    /**
     * We can use here any resolving techniques:
     * 1. Lex
     * 2. Recency
     * 3. MEA
     * 4. Refactor
     * 5. Priority wise
     *
     *  Here we are using find first rule logic.
     * @param conflictSet
     * @return
     */
//    protected RewardsAccrualRulesDto resolve(List<RewardsAccrualRulesDto> conflictSet){
//        Optional<RewardsAccrualRulesDto> rule = conflictSet.stream()
//                .findFirst();
//        if (rule.isPresent()){
//            return rule.get();
//        }
//        return null;
//    }
    
    protected RewardsAccrualRulesDto resolve(List<RewardsAccrualRulesDto> conflictSet) {
        if (conflictSet.isEmpty()) {
            return null;
        }

        // Sort the conflictSet based on priority in ascending order
        conflictSet.sort(Comparator.comparingInt(RewardsAccrualRulesDto::getPriority));

        // Get the rule with the lowest priority (first element after sorting)
        return conflictSet.get(0);
    }

    /**
     * Execute selected rule on input data.
     * @param rule
     * @param inputData
     * @return
     */
    protected O executeRule(RewardsAccrualRulesDto rule, I inputData){
        O outputResult = initializeOutputResult();
        return ruleParser.parseAction(rule.getAction(), inputData, outputResult);
        //take corersponding rule from org_accrual rules and based on the records calculations are made
        //result will save in user reward accrual
    }

    protected abstract O initializeOutputResult();
    protected abstract RuleNamespace getRuleNamespace();
}
