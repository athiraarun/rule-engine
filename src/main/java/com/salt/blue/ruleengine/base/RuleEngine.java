package com.salt.blue.ruleengine.base;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.salt.blue.macro.RewardsService;
import com.salt.blue.pojo.RewardsAccrualRulesDto;
import com.salt.frame.pojo.exception.FrameException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RuleEngine {

    @Autowired
    private RewardsService rewardsService;

    public <I, O> Object run(InferenceEngine<I, O> inferenceEngine, I inputData) throws FrameException {
    	log.info("Accessed run @ {}", new Date());
		log.debug("inferenceEngine: {}, inputData: {}", inferenceEngine, inputData);
		
        String ruleNamespace = inferenceEngine.getRuleNamespace().toString();
        //TODO: Here for each call, we are fetching all rules from db. It should be cache.
        List<RewardsAccrualRulesDto> allRulesByNamespace = rewardsService.getAllRuleByNamespace(ruleNamespace);
        Object result = inferenceEngine.run(allRulesByNamespace, inputData);
        
        log.debug("result: {}", result);
		log.info("Exited 'run' @ {}", new Date());
        return result;
    }

}
