package com.salt.blue.ruleengine;

import org.springframework.stereotype.Service;

import com.salt.blue.api.RuleNamespace;
import com.salt.blue.pojo.RewardInputDetails;
import com.salt.blue.pojo.RewardOutputDetails;
import com.salt.blue.ruleengine.base.InferenceEngine;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RewardInferenceEngine extends InferenceEngine<RewardInputDetails, RewardOutputDetails> {

    @Override
    protected RuleNamespace getRuleNamespace() {
        return RuleNamespace.REWARD;
    }

    @Override
    protected RewardOutputDetails initializeOutputResult() {
        return RewardOutputDetails.builder().build();
    }
}
