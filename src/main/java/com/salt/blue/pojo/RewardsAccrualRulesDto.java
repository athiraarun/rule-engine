package com.salt.blue.pojo;

import com.salt.blue.api.RuleNamespace;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class RewardsAccrualRulesDto {
	private long  id;
    private RuleNamespace ruleNamespace;
    private String ruleName;
    private String evaluationExpression;
    private String action;
    private Short priority;
    private String description;
}
