package com.salt.blue.pojo;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RewardOutputDetails {
	
	    private Long userId;
	    private Long accrualRuleId;
	    private Date accrualDate;
	    private Long rewardCoinId;
	    private short numberOfCoinsAccrued;
}
