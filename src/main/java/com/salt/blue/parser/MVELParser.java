package com.salt.blue.parser;

import java.util.Date;
import java.util.Map;

import org.mvel2.MVEL;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MVELParser {

    public Boolean parseMvelExpression( String expression, Map<String, Object> inputObjects){
    	log.info("Accessed parseMvelExpression @ {}", new Date());
		log.debug("expression: {}, inputObjects: {}", expression, inputObjects);
		Boolean expressionEvaluationResult = false;
        try {
        	expressionEvaluationResult = MVEL.evalToBoolean(expression,inputObjects);
            
        }catch (Exception e){
        	e.printStackTrace();
            log.error("Can not parse Mvel Expression : {} Error: {}", expression, e.getMessage());
        }
        log.debug("expressionEvaluationResult: {}", expressionEvaluationResult);
		log.info("Exited 'parseMvelExpression' @ {}", new Date());
        return expressionEvaluationResult;
    }
}
