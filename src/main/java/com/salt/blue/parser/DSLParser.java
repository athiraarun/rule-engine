package com.salt.blue.parser;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.salt.blue.resolver.dsl.base.DSLKeywordResolver;
import com.salt.blue.resolver.dsl.base.DSLResolver;
import com.salt.blue.util.DSLPatternUtil;
import com.salt.frame.pojo.exception.FrameException;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class DSLParser {

	@Autowired
	private DSLKeywordResolver keywordResolver;
	@Autowired
	private DSLPatternUtil dslPatternUtil;

	public <I> String resolveDomainSpecificKeywords(String expression, I inputData) {
		Map<String, Object> dslKeywordToResolverValueMap = executeDSLResolver(expression, inputData);
		return replaceKeywordsWithValue(expression, dslKeywordToResolverValueMap);
	}

	private <I> Map<String, Object> executeDSLResolver(String expression, I inputData) {
		List<String> listOfDslKeyword = dslPatternUtil.getListOfDslKeywords(expression);
		Map<String, Object> dslKeywordToResolverValueMap = new HashMap<>();
		listOfDslKeyword.stream().forEach(dslKeyword -> {
			Object resolveValue = null;
			try {
				String extractedDslKeyword = dslPatternUtil.extractKeyword(dslKeyword);
				String keyResolver = dslPatternUtil.getKeywordResolver(extractedDslKeyword);
				String keywordValue = dslPatternUtil.getKeywordValue(extractedDslKeyword);
				DSLResolver resolver = keywordResolver.getResolver(keyResolver).get();
				resolveValue = resolver.resolveValue(keywordValue, inputData);
			} catch (FrameException e) {
				e.printStackTrace();
			} finally {
				dslKeywordToResolverValueMap.put(dslKeyword, resolveValue);
			}
		});
		return dslKeywordToResolverValueMap;
	}

	private String replaceKeywordsWithValue(String expression, Map<String, Object> dslKeywordToResolverValueMap) {
		List<String> keyList = dslKeywordToResolverValueMap.keySet().stream().collect(Collectors.toList());
		for (int index = 0; index < keyList.size(); index++) {
			String key = keyList.get(index);
			if (dslKeywordToResolverValueMap.get(key) != null) {
				String dslResolveValue = dslKeywordToResolverValueMap.get(key).toString();
				expression = expression.replace(key, dslResolveValue);
			}
		}
		return expression;
	}
}
