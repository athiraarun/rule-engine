package com.salt.blue.repo.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceException;
import javax.persistence.QueryTimeoutException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;

import com.salt.blue.repo.entity.LoginAuditEntity;
import com.salt.blue.repo.entity.LoginAuditEntity;
import com.salt.frame.config.FWEXCEPTIONS;
import com.salt.frame.pojo.exception.FrameException;
import com.salt.frame.repo.dao.base.FrameDao;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class LoginAuditDao extends FrameDao<LoginAuditEntity> {
	@Override
	protected void setEntity() {
		this.entity = LoginAuditEntity.class;
	}

	public List<LoginAuditEntity> findByUserId(long userid) throws FrameException {
		log.info("Accessed findByUserId @ {}", new Date());

		CriteriaBuilder cb = em.getCriteriaBuilder();
		List<Predicate> preds = new ArrayList<>();
		List<LoginAuditEntity> loginAuditEntities = null;
		try {
			CriteriaQuery<LoginAuditEntity> cq = cb.createQuery(entity);
			Root<LoginAuditEntity> from = cq.from(entity);
			preds.add(cb.equal(from.get("userId"), userid));

			cq.select(from).where(preds.toArray(new Predicate[] {}));
			TypedQuery<LoginAuditEntity> q = em.createQuery(cq);

			loginAuditEntities = q.getResultList();
		} catch (NoResultException sw1) {
			log.info("Swallowed DB Error({}): record not found", entity.getName());

		} catch (NonUniqueResultException nu) {
			log.error("More than one record found", nu);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_NONUNIQUE,
					"Data Inconsistency", "There is a inconsistency in data", "Please report this error");

		} catch (QueryTimeoutException qt) {
			log.error("More than one record found", qt);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_PARTUPDATE,
					"Query Timeout", "The query timed out",
					"Please try again or exit the application and re-login, this may be a temporary glitch");

		} catch (PersistenceException thrw) {
			log.error("DB Error", thrw);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in behaviour",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		} catch (Exception e) {
			log.error("SQL EXCEPTION", e);
			throw new FrameException(HttpStatus.INTERNAL_SERVER_ERROR, FWEXCEPTIONS.EXCEPTION_DB_UNKNOWN,
					"Data Inconsistency", "There is a inconsistency in data",
					"Please try again or exit the application and re-login, this may be a temporary glitch");
		}
		log.info("Exited 'findByUserId' @ {} with result: {}", new Date(), loginAuditEntities);

		return loginAuditEntities;

	}

}
