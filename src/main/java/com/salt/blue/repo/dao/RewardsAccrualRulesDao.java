package com.salt.blue.repo.dao;

import org.springframework.stereotype.Repository;

import com.salt.blue.repo.entity.RewardsAccrualRulesEntity;
import com.salt.frame.repo.dao.base.FrameDao;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class RewardsAccrualRulesDao extends FrameDao<RewardsAccrualRulesEntity> {
    @Override
    protected void setEntity() {
        this.entity = RewardsAccrualRulesEntity.class;
    }
    
}
