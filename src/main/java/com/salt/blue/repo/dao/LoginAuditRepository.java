package com.salt.blue.repo.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.salt.blue.repo.entity.LoginAuditEntity;

@Repository
public interface LoginAuditRepository extends JpaRepository<LoginAuditEntity, Long> {
 
	List<LoginAuditEntity> findByUserId(Long userId);  
}
