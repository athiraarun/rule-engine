package com.salt.blue.repo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.salt.frame.config.FrameConfig;
import com.salt.frame.repo.entity.FrameEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "rewards_accrual_rules")
public class RewardsAccrualRulesEntity extends FrameEntity{
	
    @Column(name = "rule_namespace", length=256)
    private String ruleNamespace;
    
    @Column(name = "rule_name", length=100)
    private String rule_name;
    

    @Column(name = "evaluation_expression", columnDefinition = FrameConfig.COLDEF_MTEXT)
    private String evaluationExpression;

    @Column(name = "action", columnDefinition = FrameConfig.COLDEF_MTEXT)
    private String action;

    @Column(name = "priority")
    private Short priority;

    @Column(name = "rule_desc", columnDefinition = FrameConfig.COLDEF_MTEXT)
    private String description;

}
