package com.salt.blue.repo.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.salt.frame.repo.entity.FrameEntity;

@Entity
@Table(name = "login_audit")
public class LoginAuditEntity extends FrameEntity{

    @Column(name = "user_id")
    private Long userId;

    @Column(name = "login_time")
    private Date loginTime;

    @Column(name = "logout_time")
    private Date logoutTime;

    @Column(name = "ip_address")
    private String ipAddress;

    @Column(name = "user_agent")
    private String userAgent;

    @Column(name = "device_info")
    private String deviceInfo;

}
